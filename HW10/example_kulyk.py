# Task 1
# Створіть клас "Транспортний засіб" та підкласи "Автомобіль", "Літак", "Корабель",
# наслідувані від "Транспортний засіб". Наповніть класи атрибутами на свій розсуд.
# Створіть обʼєкти класів "Автомобіль", "Літак", "Корабель".


class Vehicle:
    category_vehicle = ('cargo', 'passenger', 'special purpose')


class Car(Vehicle):
    engine_type = ('ICE', 'electric', 'hybrid')
    chassis_type = ('framed', 'frameless')
    number_driving_axles = ('4x2', '4x4', '6x6', '8x8')
    load_capacity = ('small class: up to 3 tons', 'middle class: 3-5 tons', 'large class: above 5 tons.')
    brand = 'all brand'
    model = 'all model'
    color = 'all color'

    def __init__(self, car_brand, car_model, car_color):
        """
        Initializer, creates a class variable
        Args:
            car_brand (str): Any string describing the brand
            car_model (str): Any string describing the model
            car_color (str): Any string describing the color
        """
        self.brand = car_brand
        self.model = car_model
        self.color = car_color


class Aircraft(Vehicle):
    wings_type = ('fixed wings', 'movable wings')
    engine_type = ('piston', 'combined', 'turbojet', 'turboprop', 'rocket')
    number_engines = ('single-engine', 'twin-engine', 'multi-engine')
    landing_gear = ('wheel landing gear', 'caterpillar landing gear', 'ski landing gear', 'float landing gear')
    brand = 'all brand'
    model = 'all model'
    color = 'all color'

    def __init__(self, air_brand, air_model, air_color):
        """
        Initializer, creates a class variable
        Args:
            air_brand (str): Any string describing the brand
            air_model (str): Any string describing the model
            air_color (str): Any string describing the color
        """
        self.brand = air_brand
        self.model = air_model
        self.color = air_color


class Ship(Vehicle):
    by_navigation_area = ('sea', 'inland navigation', 'mixed navigation')
    engine_type = ('ICE', 'gas turbine engines', 'steam turbine engines')
    ship_size = ('Handysize', 'Handymax', 'Seawaymax', 'Aframax', 'Panamax')
    number_propellers = ('single-shaft', 'two-shaft', 'three-shaft', 'four-shaft')
    brand = 'all brand'
    model = 'all model'
    color = 'all color'

    def __init__(self, ship_brand, ship_model, ship_color):
        """
        Initializer, creates a class variable
        Args:
            ship_brand (str): Any string describing the brand
            ship_model (str): Any string describing the model
            ship_color (str): Any string describing the color
        """
        self.brand = ship_brand
        self.model = ship_model
        self.color = ship_color


car = Car(car_brand='Kia', car_model='Sportage 3', car_color='Black')
aircraft = Aircraft(air_brand='Embraer', air_model='Legacy 600', air_color='black/white')
ship = Ship(ship_brand='USS Missouri', ship_model='BB-63', ship_color='grey')

print(f'Brand - {car.brand}\n'
      f'Model - {car.model}\n'
      f'Color - {car.color}\n'
      f'Type engine - {car.engine_type[0]}\n'
      f'Type Chassis - {car.chassis_type[1]}\n'
      f'Type drive - {car.number_driving_axles[1]}\n'
      f'Load capacity - {car.load_capacity[0]}\n'
      f'Category vehicle - {car.category_vehicle[1]}')
