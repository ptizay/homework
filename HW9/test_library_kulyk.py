import library_kulyk


assert library_kulyk.is_string_capitalized('My name is David') is False
assert library_kulyk.is_string_capitalized('I love playing') is True
assert library_kulyk.is_string_capitalized('') is True
assert library_kulyk.is_string_capitalized(' ') is True
assert library_kulyk.is_string_capitalized('565656') is True

assert library_kulyk.is_even_number(4) is True
assert library_kulyk.is_even_number(3) is False
assert library_kulyk.is_even_number(2.0) is True
assert library_kulyk.is_even_number(2.2) is False
assert library_kulyk.is_even_number(0) is True
