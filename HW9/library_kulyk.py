# Task 1
# Функцію is_string_capitalized потрібно дописати
# (в тому числі докстрінги) - очікування - прохід тестів.


def is_string_capitalized(string):
    """
    Checks if the given string is capitalized
    Args:
        string (str): any string
    Returns:
        True|False
    """
    string = string.strip()
    if string == string.capitalize():
        return True
    return False


res_task1 = is_string_capitalized(' Asd')
print('Task 1 result -', res_task1)


# Task 2
# Напишіть функцію, яка перевірить, чи дане число є парним чи непарним
# (коментар з коду видаліть) ТА ПОКРИЙТЕ ЇЇ ТЕСТАМИ.

def is_even_number(number):
    """
    Checks if the given number is even or odd
    Args:
        number (int, float): any number
    Returns:
        True|False
    """
    if number % 2 == 0:
        return True
    else:
        return False


res_task2 = is_even_number(2.0)
print('Task 2 result -', res_task2)
