# Task 1
# Доопрацюйте класс Point так, щоб в атрибути x та y обʼєктів цього класу можна було записати
# тільки обʼєкти класу int або float.
# Task 2
# Створіть класс Triangle (трикутник), який задається трьома точками (обʼєкти классу Point).
# Реалізуйте перевірку даних, аналогічно до класу Line.
# Визначет атрибут, що містить площу трикутника (за допомогою property).
# Для обчислень можна використати формулу Герона (https://en.wikipedia.org/wiki/Heron%27s_formula)


class Point:
    _x = 0
    _y = 0

    def __init__(self, x, y):
        """
        Initializer, creates a class variable
        Args:
            x (int|float): Any int or float number
            y (int|float): Any int or float number
        """
        self.x = x
        self.y = y

    @property
    def x(self):
        """
        Reading a class variable
        Returns: int | float
        """
        return self._x

    @property
    def y(self):
        """
        Reading a class variable
        Returns: int | float
        """
        return self._y

    @x.setter
    def x(self, value):
        """
        Entry with variable check
        Args:
            value (int|float): Any int or float number
        """

        if not isinstance(value, (int, float)):
            raise TypeError('Coordinate number is not int or float!')
        self._x = value

    @y.setter
    def y(self, value):
        """
        Entry with variable check
        Args:
            value (int|float): Any int or float number
        """
        if not isinstance(value, (int, float)):
            raise TypeError('Coordinate number is not int or float!')
        self._y = value


class Triangle:
    _p1 = None
    _p2 = None
    _p3 = None

    def __init__(self, apex_1, apex_2, apex_3):
        """
        Initializer, creates a class variable
        Args:
            apex_1 (object): Any object of class Point
            apex_2 (object): Any object of class Point
            apex_3 (object): Any object of class Point
        """
        self.p1 = apex_1
        self.p2 = apex_2
        self.p3 = apex_3

    @property
    def length_sides(self):
        """
        Calculates the lengths of the sides of a triangle from points
        Returns: (tuple) collection with three lengths
        """
        cathet_1 = self.p1.x - self.p2.x
        cathet_2 = self.p1.y - self.p2.y
        cathet_3 = self.p2.x - self.p3.x
        cathet_4 = self.p2.y - self.p3.y
        cathet_5 = self.p3.x - self.p1.x
        cathet_6 = self.p3.y - self.p1.y

        length_1 = (cathet_1 ** 2 + cathet_2 ** 2) ** 0.5
        length_2 = (cathet_3 ** 2 + cathet_4 ** 2) ** 0.5
        length_3 = (cathet_5 ** 2 + cathet_6 ** 2) ** 0.5

        return length_1, length_2, length_3

    @property
    def semi_per(self):
        """
        Calculates the semi-perimeter of a triangle
        Returns: float
        """
        sum_of_sides = self.length_sides[0] + self.length_sides[1] + self.length_sides[2]

        semi_per = sum_of_sides / 2

        return semi_per

    @property
    def area(self):
        """
        Calculates the area of a triangle from Heron's shape
        Returns: float
        """
        diff_semi_per_side_1 = self.semi_per - self.length_sides[0]
        diff_semi_per_side_2 = self.semi_per - self.length_sides[1]
        diff_semi_per_side_3 = self.semi_per - self.length_sides[2]

        diff_semi_per_sides = diff_semi_per_side_1 * diff_semi_per_side_2 * diff_semi_per_side_3

        area = (self.semi_per * diff_semi_per_sides) ** 0.5

        return area

    @property
    def p1(self):
        """
        Reading a class variable
        Returns: object
        """
        return self._p1

    @p1.setter
    def p1(self, value):
        """
        Entry with class variable check
        Args:
            value (object): Any object of class Point
        """
        if not isinstance(value, Point):
            raise TypeError('Attribute is not a class Point!')
        self._p1 = value

    @property
    def p2(self):
        """
        Reading a class variable
        Returns: object
        """
        return self._p2

    @p2.setter
    def p2(self, value):
        """
        Entry with class variable check
        Args:
            value (object): Any object of class Point
        """
        if not isinstance(value, Point):
            raise TypeError('Attribute is not a class Point!')
        self._p2 = value

    @property
    def p3(self):
        """
        Reading a class variable
        Returns: object
        """
        return self._p3

    @p3.setter
    def p3(self, value):
        """
        Entry with class variable check
        Args:
            value (object): Any object of class Point
        """
        if not isinstance(value, Point):
            raise TypeError('Attribute is not a class Point!')
        self._p3 = value


p1 = Point(4, 0)
p2 = Point(0, 3)
p3 = Point(4, 3)

tri = Triangle(p1, p2, p3)

print(f'Points: A{p1.x, p1.y}, B{p2.x, p2.y}, C{p3.x, p3.y}')
print(f'The lengths of the sides of a triangle - {tri.length_sides[0]}, {tri.length_sides[1]}, {tri.length_sides[2]}')
print(f'The area of the triangle is {tri.area}')
