# Task 1
# Дана довільна строка. Напишіть код, який знайде в ній і виведе на екран кількість слів,
# які містять дві голосні літери підряд.

my_str = 'Його та моя координація була не дуже, тому ми не пройшли це випробування.'
vowels = 'aeiouyаеийіоуяюєї'
low_my_str = my_str.lower()
my_list = list(low_my_str.split(sep=' '))
count_word = 0

for word in my_list:                                        # word iteration
    for let in range(len(word)-1):                          # iterate over word length
        letter = let
        count_repeat_1 = 0                                  # first vowel match counter
        count_repeat_2 = 0                                  # second vowel match counter
        for vwl in vowels:                                  # iterate a string of vowels
            vwl_rep = vwl
            if word[letter].count(vwl_rep):                 # first vowel match check
                count_repeat_1 += 1
            if word[letter+1].count(vwl_rep):               # check for matching following vowel
                count_repeat_2 += 1
                continue
            else:
                continue
        if count_repeat_1 > 0 and count_repeat_2 > 0:       # checking vowel pairs
            count_word += 1
            break
        else:
            continue
print(count_word, '- слово(a)/слів з двома голосними літерами підряд.')


# Task 2
# Є два довільних числа які відповідають за мінімальну і максимальну ціну.
# Є Dict з назвами магазинів і цінами: { "cito": 47.999, "BB_studio" 42.999, "momo": 49.999, "main-service": 37.245,
# "buy.now": 38.324, "x-store": 37.166, "the_partner": 38.988, "store": 37.720, "rozetka": 38.003}. Напишіть код,
# який знайде і виведе на екран назви магазинів, ціни яких попадають в діапазон між мінімальною
# і максимальною ціною. Наприклад:
# lower_limit = 35.9
# upper_limit = 37.339
# > match: "x-store", "main-service"

min_price = 35.9
max_price = 37.339
my_dict = {
    'city': 47.999,
    'BB_studio': 42.999,
    'momo': 49.999,
    'main-service': 37.245,
    'buy.now': 38.324,
    'x-store': 37.166,
    'the_partner': 38.988,
    'store': 37.720,
    'rozetka': 38.003,
}

for price in my_dict.items():
    if min_price < price[1] < max_price:
        print(price[0])
