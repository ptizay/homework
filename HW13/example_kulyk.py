# Task
# Підключіться до API НБУ ( документація тут https://bank.gov.ua/ua/open-data/api-dev ),
# отримайте курс валют і запишіть його в текстовий файл, в такому форматі (список):
#   "[дата створення запиту]"
#   1. [назва валюти 1] to UAH: [значення курсу до валюти 1]
#   2. [назва валюти 2] to UAH: [значення курсу до валюти 2]
#   3. [назва валюти 3] to UAH: [значення курсу до валюти 3]
#   ...
#   n. [назва валюти n] to UAH: [значення курсу до валюти n]
# P.S.не забувайте про DRY, KISS, SRP та перевірки.

import requests

nbu_url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json'


def check_request(url):
    """
    URL Accessibility Check
    Args:
        url (str): Any URL
    Returns: response object class requests
    """
    try:
        result = requests.request(method='GET', url=url)
    except:
        result = None
    return result


def check_status_code(response):
    """
    Check status code
    Args:
        response: Any response
    Returns: response object class requests
    """
    if 200 <= response.status_code < 300:
        result = response
        return result
    else:
        print(f'{response.status_code} - status code, it is not normal!')


def check_response_headers(response):
    """
    Checking the response for the presence of the required Content-Type
    Args:
        response: Any response
    Returns: response object class requests
    """
    if response.headers['Content-Type'] == 'application/json; charset=utf-8':
        result = response
        return result
    else:
        print('Headers don\'t have \'Content-Type\' - \'app/json\'')


def check_response_for_json(response):
    """
    Checking response format - json
    Args:
        response: Any response
    Returns: list
    """
    try:
        response_json = response.json()
    except:
        response_json = None
    return response_json


nbu_api_request = check_request(url=nbu_url)
nbu_api_request = check_status_code(response=nbu_api_request)
nbu_api_request = check_response_headers(response=nbu_api_request)
nbu_exchange_rate_json = check_response_for_json(response=nbu_api_request)

with open('nbu_exchange_rate.txt', 'w') as file:
    file.write(nbu_api_request.headers.get('Date'))
    count = 1
    for elem in nbu_exchange_rate_json:
        currency_name = elem.get('txt', {})
        currency_value = elem.get('rate', {})
        file.write(f'\n{count}. {currency_name} to UAH: {currency_value}')
        count += 1
