# Task 1
# Є дікт, невідомого наповнення. В дікті присутні ключі, значенням для яких є дікти невідомого наповнення
# в яких можуть бути аналогічні вкладені дікти. Напишіть функцію, яка дістане всі ключі зі значеннями не-діктами
# з усіх рівнів вкладення, помістить на один рівень в окремий дікт і поверне цей дікт.

# Task 2
# Напишіть докстрінг для цієї функці.

from pprint import pprint

my_dict = {
    'name': 'Alex',
    'age': '25',
    'skill': {
        'skill_1': {
            'drive_cat1': 'A',
            'drive_cat2': 'B',
        },
        'skill_2': {
            'fishing_exp_sport': '1 year',
            'fishing_exp_hobby': '3 years'
        },
        'Skill_3': 'gamer',
        'Skill_4': 'builder',
    },
    'friends': {
        'Tommy': 'age 22',
        'Grem': 'age 25',
        'Lisa': 'age 23',
        }
    }


def simple_key(iter_dict):
    """
        Функція здійснює пошук у глибину.
        Дістає всі ключі зі значеннями (які не є вкладеними диктами)
        з усіх рівнів вкладень та формує однорівневий дикт (ключ : значення)
        Args:
            iter_dict(dict): багаторівневий dict, в якому функція здійснює пошук
        Returns:
            new_dict(dict): однорівневий dict, результат роботи функції simple_key
        """
    new_dict = {}
    for k, v in iter_dict.items():
        if isinstance(v, dict):
            new_dict.update(simple_key(v))
        else:
            new_dict.setdefault(k, v)
    return new_dict


dict_result = simple_key(iter_dict=my_dict)
pprint(dict_result)
