# Task 1
# URL http://api.open-notify.org/astros.json
# Вивести список всіх астронавтів, що перебувають в даний момент на орбіті
# (дані не фейкові, оновлюються в режимі реального часу).

import requests

url = 'http://api.open-notify.org/astros.json'

response = requests.get(url=url)
space_info = response.json()
astro = space_info['people']
num_people = space_info['number']

for count in range(num_people):
    print(astro[count]['name'])
print('Total number of people in Earth`s orbit -', num_people)


# Task 2
# api погоди (всі токени я для вас вже прописав):
# https://api.openweathermap.org/data/2.5/weather?q={city_name}&appid=47503e85fabbabc93cff28c52398ae97&units=metric
# де city_name - назва міста на аглійській мові (наприклад, odesa, kyiv, lviv, london).
# Результатом буде приблизно такий результат:
#       {"coord":{"lon":30.7326,"lat":46.4775},"weather":[{"id":803,"main":"Clouds","description":"broken clouds",
#       "icon":"04n"}],"base":"stations","main":{"temp":13.94,"feels_like":12.8,"temp_min":13.94,"temp_max":13.94,
#       "pressure":1021,"humidity":54,"sea_level":1021,"grnd_level":1015},"visibility":10000,"wind":{"speed":4.58,
#       "deg":314,"gust":8.16},"clouds":{"all":73},"dt":1664909335,"sys":{"country":"UA","sunrise":1664855942,
#       "sunset":1664897549},"timezone":10800,"id":698740,"name":"Odesa","cod":200}
# Погода змінюється, як і місто, яке ви введете.
# Роздрукувати температуру та швидкість вітру, з вказівкою міста, яке було вибране.

city = input('Enter the name of the city (for example, odesa, kyiv, lviv, london, etc.): ')

url = '''
https://api.openweathermap.org/data/2.5/weather?q={city_name}&appid=47503e85fabbabc93cff28c52398ae97&units=metric
'''

response = requests.get(url=url.format(city_name=city))
weather = response.json()
name_city = weather['name']
temperature = weather['main']['temp']
temp_celsius = int(temperature - 273.15)                # temperature conversion from kelvin to celsius
wind_speed = weather['wind']['speed']

print(f'The current weather in {name_city.title()}:')
print(f'Air temperature: {temp_celsius} °C.')
print(f'Wind speed: {wind_speed} m/s.')
