def any_int():
    """
        Функція нічого не приймає,
        а вибирає випадкове ціле число від "1" до "100" та зберігає у змінну.
        Returns:
            ai_num(int): випадкове ціле число
        """
    from random import randint
    ai_num = randint(1, 100)
    return ai_num


def get_num(msg):
    """
        Функція приймає з клавіатури строку,
        перевіряє, перетворює str на позитивне ціле число та повертає його.
        Args:
            msg(str): дані отримуються с клавіатури
        Returns:
            user_num(int): позитивне ціле число
        """
    while True:
        try:
            user_num = abs(int(input(msg)))
            return user_num
        except ValueError:
            print('Це не є цілим числом! Спробуй ще!')
        except:
            print('Помилка! Щось пішло не так.')


def num_comparison(ai_num, user_num):
    """
        Функція приймає два позитивних цілих числа,
        визначає близькість одного цілого числа
        до іншого з виведенням відповідного коментаря
        Args:
            ai_num(int): позитивне ціле число
            user_num(int): позитивне ціле число
    """
    if user_num == 0 or user_num > 100:
        print('Не туди! Я загадав від 1 до 100 ;)')
    elif abs(user_num - ai_num) > 10:
        print('Холодно!')
    elif 5 <= abs(user_num - ai_num) <= 10:
        print('Тепло!')
    elif 1 <= abs(user_num - ai_num) <= 4:
        print('Гаряче!')


def question_to_user():
    """
        Функція виводить str(питання) та аналізу відповіді (переводить у нижній регістр і
        умовно перевіряє входження до заданої колекції).
    Returns:
        Повертає str-відповідь
    """
    while True:
        answer = input('Хочешь повторити гру? Y/N: ')
        if answer.lower() in ('yes', 'y', 'так'):
            return answer.lower()
        elif answer.lower() in ('no', 'n', 'ні'):
            return answer.lower()
        else:
            print('Зроби вибір так(Y) чи ні(N)!')
            continue
