# Task 1
# Напишіть гру "Вгадай число":
#   a. програма загадує число від 1 до 100 і користувач намагається його вгадати.
#       - якщо вгадав - вивести повідомлення "Поздоровляємо!"
#       - якщо невгадав і різниця між числом користувача, і загаданим, більше 10 - вивести
#         повідомлення "Холодно", якщо 5-10 - "Тепло", якщо 1-4 "Гаряче".
#   b. після того, як користувач вгадав число - запитати, чи хоче він повторити гру (Y\N).
#   c. всі функцій, крім основної, помістіть в окремий файл і імпортуйте в основний файл.
#
# Task 2
# Напишіть декоратор, який вимірює і виводить на екран час виконання функції в секундах і задекоруйте ним
# основну функцію гри. Після закінчення гри декоратор має сповістити, скільки тривала гра.

import storage_func
import time


def decor_stopwatch(func):
    """
        Функція декоратор, яка вимірює час виконання функції аргумента
            Args:
                func(function): Функція час виконання якої буде вимірюваться функцією декоратором
            Returns:
                wrapper(function): результат виконання функції обгортки
    """
    def wrapper(*args, **kwargs):
        """
           Модифікує та розраховує час виконання функції
        Args:
            *args (Any): any data
            **kwargs (Any): any data
        Returns:
            Повертаю результат та час виконання func()
        """
        start_game = time.time()
        play_game = func(*args, **kwargs)
        end_game = round(time.time() - start_game)
        print(f'Наша гра зайняла {end_game} сек.!')
        return play_game
    return wrapper


@decor_stopwatch
def game():
    """
        Основна функція-гра "Вгадай число" яка загадує число в діапозоні від 1 до 100 та
        просить користувача вгадати це число.
    """
    print('Привіт! Давай пограємо в гру "Вгадай число".')
    while True:
        print('Я загадав число від 1 до 100!')
        ai = storage_func.any_int()
        while True:
            user_num = storage_func.get_num(msg='Вгадай число, яке я загадав: ')
            if user_num == ai:
                print('Поздоровляємо! Ти вгадав!')
                break
            else:
                storage_func.num_comparison(ai_num=ai, user_num=user_num)
                continue
        user_answer = storage_func.question_to_user()
        if user_answer in ('yes', 'y', 'так'):
            continue
        else:
            print('Добре пограли! Успіхів!')
            break


game()
