# Task1
# Зформуйте строку, яка містить певну інформацію про символ по його індексу в відомому слові. Наприклад "The
# [індекс символу] symbol in [тут слово] is '[символ з відповідним порядковим номером]'". Слово та номер отримайте
# за допомогою input() або скористайтеся константою.
# Наприклад (слово - "Python" а номер символу 3) - "The 3 symbol in "Python" is 't' ".

while True:
    user_input_num = input('Введіть любе ціле додатне число: ')
    if user_input_num.isdigit() and int(user_input_num) > 0:
        user_input_res_num = int(user_input_num)
        break
    print(f'\"{user_input_num}\" не є цілим або додатним числом! Будьте уважні!')

while True:
    user_input_wrd = input('Введіть любе слово: ')
    if user_input_wrd.isalpha():
        user_input_res_wrd = user_input_wrd
        break
    print(f'\"{user_input_wrd}\" не є словом!')

if user_input_res_num <= len(user_input_res_wrd):
    letter = user_input_res_wrd[user_input_res_num - 1]
    result = f'\"The {user_input_res_num} symbol in \"{user_input_res_wrd}\" is \'{letter}\'\".'
    print(result)
    print(type(len(user_input_res_wrd)))
else:
    print(f'В слові \"{user_input_res_wrd}\" менше ніж \"{user_input_res_num}\" літер.')


# Task 2
# Вести з консолі строку зі слів за допомогою input() (або скористайтеся константою).
# Напишіть код, який визначить кількість слів, в цих даних.

sentence = input('Введіть речення або набір слів через пробіл: ')
world_count = len(sentence.split())
print(world_count)


# Task 3
# Існує ліст з різними даними, наприклад
# lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишіть код, який сформує новий list (наприклад lst2), який би містив всі числові змінні (int, float), які є в lst1.
# Майте на увазі, що данні в lst1 не є статичними можуть змінюватись від запуску до запуску.

lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
lst2 = []
for elem_lst in lst1:
    if type(elem_lst) == int or type(elem_lst) == float:
        lst2.append(elem_lst)
print(lst1, id(lst1))
print(lst2, id(lst2))
