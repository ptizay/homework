# Task 1
# Напишіть функцію, що приймає один аргумент будь-якого типу та повертає цей аргумент, перетворений на float.
# Якщо перетворити не вдається функція має повернути 0.

def input_to_float(msg):
    try:
        return float(input(msg))
    except ValueError:
        return 0


result = input_to_float(msg='Enter something: ')
print(result)


# Task 2
# Напишіть функцію, що приймає два аргументи. Функція повинна:
# - якщо аргументи відносяться до числових типів (int, float) - повернути перемножене значення цих аргументів,
# - якщо обидва аргументи це строки (str) - обʼєднати в одну строку та повернути
# - у будь-якому іншому випадку повернути кортеж з цих аргументів

def fun_combine(arg1, arg2):
    if type(arg1) is str and type(arg2) is str:
        concat = arg1 + arg2
        return concat
    elif (type(arg1) is int or type(arg1) is float) and (type(arg2) is int or type(arg2) is float):
        multiplic = arg1 * arg2
        return multiplic
    else:
        my_tuple = (arg1, arg2)
        return my_tuple


result = fun_combine(arg1=-4.8, arg2='asd')
print(result)


# Task 3
# Перепишіть за допомогою функцій вашу программу "Касир в кінотеатрі", яка буде виконувати наступне:
# Попросіть користувача ввести свій вік.
# - якщо користувачу менше 7 - вивести "Тобі ж <> <>! Де твої батьки?"
# - якщо користувачу менше 16 - вивести "Тобі лише <> <>, а це е фільм для дорослих!"
# - якщо користувачу більше 65 - вивести "Вам <> <>? Покажіть пенсійне посвідчення!"
# - якщо вік користувача містить 7 - вивести "Вам <> <>, вам пощастить"
# - у будь-якому іншому випадку - вивести "Незважаючи на те, що вам <> <>, білетів всеодно нема!"
# Замість <> <> в кожну відповідь підставте значення віку (цифру) та правильну форму слова рік.
# Для будь-якої відповіді форма слова "рік" має відповідати значенню віку користувача (1 - рік, 22 - роки,
# 35 - років і тд...).
# Наприклад :
# "Тобі ж 5 років! Де твої батьки?"
# "Вам 81 рік? Покажіть пенсійне посвідчення!"
# "Незважаючи на те, що вам 42 роки, білетів всеодно нема!"

def get_age(msg):
    while True:
        try:
            return abs(int(input(msg)))
        except ValueError:
            print('Вік вказаний некоректно!')
        except:
            print('Помилка! Щось пішло не так.')


def exam_age(age):
    if age < 7:
        msg = f'Тобі ж {age} {word}! Де твої батьки?'
    elif 7 < age < 16:
        msg = f'Тобі лише {age} {word}, а це є фільм для дорослих!'
    elif '7' in str(age):
        msg = f'Вам {age} {word}, вам пощастить!'
    elif 65 < age < 90:
        msg = f'Вам {age} {word}? Покажіть пенсійне посвідчення!'
    elif age >= 90:
        msg = f'Вам {age} {word}? Ви добре виглядаєте, квиток за рахунок кінотеатру! Гарного перегляду!'
    else:
        msg = f'Незважаючи на те, що вам {age} {word}, білетів всеодно нема!'
    return msg


def exam_word(age):
    if 10 <= age <= 20:
        wrd = 'років'
    elif age == 1 or str(age).endswith('1'):
        wrd = 'рік'
    elif 2 <= age <= 4 or str(age).endswith('2') or str(age).endswith('3') or str(age).endswith('4'):
        wrd = 'роки'
    else:
        wrd = 'років'
    return wrd


def send_msg(msg):
    print(msg)


user_age = get_age(msg='Привіт! Хочешь придбати квиток? Вкажіть свій вік: ')
word = exam_word(age=user_age)
message = exam_age(age=user_age)
send_msg(msg=message)
